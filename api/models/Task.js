const mongoose = require('mongoose');

const TaskSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
    },
    title: {
        type: String,
        required: true
    },
    status: {
        type: String,
        default: 'new',
        enum: ['new', 'in_progress', 'done']
    },
});

const Task = mongoose.model('Task', TaskSchema);
module.exports = Task;
