const rootPath = __dirname;

module.exports = {
    rootPath,
    mongoConfig: {
        db: 'mongodb://localhost/tasks',
        options: {useNewUrlParser: true},
    }
}
