const express = require('express');
const Task = require("../models/Task");
const mongoose = require("mongoose");

const router = express.Router();

router.get('/', async (req,res,next) => {
    try {
        const tasks = await Task.find().populate("user", "username");

        return res.send(tasks);
    } catch (e) {
        next(e);
    }
});

router.post('/', async (req, res, next) => {
    try {
        if (!req.body.title) {
            return res.status(400).send({message: 'Title is required!'});
        }

        const taskData = {
            user: req.body.user,
            title: req.body.title,
            status: "new"
        };

        const task = new Task(taskData);

        await task.save();

        return res.send(taskData);
    } catch (e) {
        next(e);
    }
});

router.put('/:id', async (req, res, next) => {
    try {
        const task = await Task.findById(req.params.id);

        if (!task) {
            return res.status(404).send({error: 'Task not found'});
        }

        if (!req.body.status && !req.body.user) {
            return res.status(400).send({error: 'status or user is required'});
        }

        if (req.body.status) {
            task.status = req.body.status;
        }

        if (req.body.user) {
            task.user = req.body.user;
        }

        await task.save();
        await task.populate('user', 'username')

        return res.send(task);
    } catch (error) {
        if (error instanceof mongoose.Error.ValidationError) {
            return res.status(400).send(error);
        }

        return next(error);
    }
});

router.delete('/:id', async (req,res,next) => {
    try {
        await Task.deleteOne({_id: req.params.id});

        return res.send({message: 'Task deleted'});
    } catch (e) {
        next(e);
    }
});

module.exports = router;
