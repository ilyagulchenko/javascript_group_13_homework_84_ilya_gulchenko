const mongoose = require('mongoose');
const config = require('./config');
const Task = require("./models/Task");
const User = require("./models/User");

const run = async () => {
    await mongoose.connect(config.mongoConfig.db, config.mongoConfig.options);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [Many, David, Lynn] = await User.create({
        username: 'Many'
    }, {
        username: 'David'
    }, {
        username: 'Lynn'
    });

    await Task.create({
        user: Many,
        title: 'Fix the car',
        status: 'in_progress',
    }, {
        user: David,
        title: 'Buy components',
        status: 'done',
    }, {
        user: Lynn,
        title: 'Go to the market by car',
        status: 'new',
    });

    await mongoose.connection.close();
};

run().catch(e => console.error(e));
