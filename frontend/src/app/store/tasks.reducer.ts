import { TasksState } from './types';
import { createReducer, on } from '@ngrx/store';
import {
  changeTasksRequest, changeTasksSuccess, createTaskFailure, createTaskRequest, createTaskSuccess,
  fetchTasksFailure,
  fetchTasksRequest,
  fetchTasksSuccess, removeTaskFailure,
  removeTaskRequest,
  removeTaskSuccess
} from './tasks.actions';

const initialState: TasksState = {
  tasks: [],
  fetchLoading: false,
  fetchError: null,
  deleteLoading: false,
  deleteError: null,
  changeLoading: false,
  changeError: null,
  createLoading: false,
  createError: null
}

export const tasksReducer = createReducer(
  initialState,
  on(fetchTasksRequest, state => ({...state, fetchLoading: true})),
  on(fetchTasksSuccess, (state, {tasks}) => ({...state, fetchLoading: false, tasks})),
  on(fetchTasksFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(removeTaskRequest, state => ({...state, deleteLoading: true})),
  on(removeTaskSuccess, state => ({...state, deleteLoading: false})),
  on(removeTaskFailure, (state, {error}) => ({...state, deleteLoading: false, deleteError: error})),

  on(changeTasksRequest, state => ({...state, changeLoading: true})),
  on(changeTasksSuccess, (state, {task}) => ({
    ...state,
    tasks: state.tasks.map(item => {
      if (item.id === task.id) {
        return task;
      }
      return item;
    })
  })),

  on(createTaskRequest, state => ({...state, createLoading: true})),
  on(createTaskSuccess, state => ({...state, createLoading: false})),
  on(createTaskFailure, (state, {error}) => ({...state, createLoading: false, createError: error})),
);
