import { createAction, props } from '@ngrx/store';
import { Task, TaskData } from '../models/task.model';

export const fetchTasksRequest = createAction('[Tasks] Fetch Request');
export const fetchTasksSuccess = createAction('[Tasks] Fetch Success', props<{tasks: Task[]}>());
export const fetchTasksFailure = createAction('[Tasks] Fetch Failure', props<{error: string}>());

export const removeTaskRequest = createAction('[Tasks] Remove Request', props<{id: string}>());
export const removeTaskSuccess = createAction('[Tasks] Remove Success');
export const removeTaskFailure = createAction('[Tasks] Remove Failure', props<{error: string}>());

export const changeTasksRequest = createAction('[Tasks] Change Request', props<{id: string, change: {user?: string, status?: string}}>());
export const changeTasksSuccess = createAction('[Tasks] Change Success', props<{task: Task}>());
export const changeTasksFailure = createAction('[Tasks] Change Failure', props<{error: string}>());

export const createTaskRequest = createAction('[Tasks] Create Request', props<{taskData: TaskData}>());
export const createTaskSuccess = createAction('[Tasks] Create Success');
export const createTaskFailure = createAction('[Tasks] Create Failure', props<{error: string}>());
