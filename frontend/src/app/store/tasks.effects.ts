import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { TasksService } from '../services/tasks.service';
import {
  changeTasksRequest, changeTasksSuccess, createTaskFailure, createTaskRequest, createTaskSuccess,
  fetchTasksFailure,
  fetchTasksRequest,
  fetchTasksSuccess,
  removeTaskFailure,
  removeTaskRequest, removeTaskSuccess
} from './tasks.actions';
import { catchError, map, mergeMap, of, tap } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class TasksEffects {
  fetchTasks = createEffect(() => this.actions.pipe(
    ofType(fetchTasksRequest),
    mergeMap(() => this.tasksService.fetchTasks().pipe(
      map(tasks => fetchTasksSuccess({tasks})),
      catchError(() => of(fetchTasksFailure({error: 'Something wrong!'})))
    ))
  ));

  createTask = createEffect(() => this.actions.pipe(
    ofType(createTaskRequest),
    mergeMap(({taskData}) => this.tasksService.addTask(taskData).pipe(
      map(() => createTaskSuccess()),
      tap(() => this.router.navigate(['/'])),
      catchError(() => of(createTaskFailure({error: 'Wrong data'})))
    ))
  ));

  changeTask = createEffect(() => this.actions.pipe(
    ofType(changeTasksRequest),
    mergeMap(({id, change}) => this.tasksService.changeTask(id, change).pipe(
      map(task => changeTasksSuccess({task}))
    ))
  ));

  removeTask = createEffect(() => this.actions.pipe(
    ofType(removeTaskRequest),
    mergeMap(({id}) => this.tasksService.removeTask(id).pipe(
      map(() => removeTaskSuccess()),
      catchError(() => of(removeTaskFailure({error: 'Something wrong'})))
    ))
  ));

  constructor(
    private actions: Actions,
    private tasksService: TasksService,
    private router: Router
  ) {}
}
