import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap, of } from 'rxjs';
import { UsersService } from '../services/users.service';
import { fetchUsersFailure, fetchUsersRequest, fetchUsersSuccess } from './users.actions';

@Injectable()
export class UsersEffects {
  fetchUsers = createEffect(() => this.actions.pipe(
    ofType(fetchUsersRequest),
    mergeMap(() => this.usersService.fetchUsers().pipe(
      map(users => fetchUsersSuccess({users})),
      catchError(() => of(fetchUsersFailure({error: 'Something wrong!'})))
    ))
  ));

  constructor(
    private actions: Actions,
    private usersService: UsersService,
  ) {}
}
