import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../../models/user.model';
import { fetchUsersRequest } from '../../store/users.actions';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { NgForm } from '@angular/forms';
import { TaskData } from '../../models/task.model';
import { createTaskRequest } from '../../store/tasks.actions';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  users: Observable<User[]>
  loading: Observable<boolean>
  error: Observable<string | null>

  constructor(
    private store: Store<AppState>,
  ) {
    this.users = store.select(state => state.users.users);
    this.loading = store.select(state => state.tasks.createLoading);
    this.error = store.select(state => state.tasks.createError);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchUsersRequest());
  }

  addNewTask() {
    const taskData: TaskData = this.form.value;
    this.store.dispatch(createTaskRequest({taskData}));
  }

}
