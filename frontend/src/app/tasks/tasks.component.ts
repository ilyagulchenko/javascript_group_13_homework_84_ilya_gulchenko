import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../store/types';
import { Observable } from 'rxjs';
import { Task } from '../models/task.model';
import { changeTasksRequest, fetchTasksRequest, removeTaskRequest } from '../store/tasks.actions';
import { User } from '../models/user.model';
import { fetchUsersRequest } from '../store/users.actions';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {
  users: Observable<User[]>
  usersLoading: Observable<boolean>
  usersError: Observable<null | string>
  tasks: Observable<Task[]>
  loading: Observable<boolean>
  error: Observable<null | string>
  removeLoading: Observable<boolean>
  removeError: Observable<null | string>

  constructor(private store: Store<AppState>) {
    this.users = store.select(state => state.users.users);
    this.usersLoading = store.select(state => state.users.fetchLoading);
    this.usersError = store.select(state => state.users.fetchError);
    this.tasks = store.select(state => state.tasks.tasks);
    this.loading = store.select(state => state.tasks.fetchLoading);
    this.error = store.select(state => state.tasks.fetchError);
    this.removeLoading = store.select(state => state.tasks.deleteLoading);
    this.removeError = store.select(state => state.tasks.deleteError);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchTasksRequest());
    this.store.dispatch(fetchUsersRequest());
  }

  changeUser(id: string, newId: string) {
    this.store.dispatch(changeTasksRequest({id, change: {user: newId}}))
  }

  changeStatus(id: string, newId: string) {
    this.store.dispatch(changeTasksRequest({id, change: {status: newId}}))
  }

  removeTask(id: string) {
    this.store.dispatch(removeTaskRequest({id}));
    this.store.dispatch(fetchTasksRequest());
  }

}
