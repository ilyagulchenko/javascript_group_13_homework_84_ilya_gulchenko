export class Task {
  constructor(
    public id: string,
    public user: {
      _id: string,
      username: string
    } | undefined,
    public title: string,
    public status: string,
  ) {}
}

export interface ApiTaskData {
  _id: string,
  user?: {
    _id: string,
    username: string
  },
  title: string,
  status: string,
}

export interface TaskData {
  title: string;
  user: string | null;
}
