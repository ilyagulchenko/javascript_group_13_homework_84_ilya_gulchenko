import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiTaskData, Task, TaskData } from '../models/task.model';
import { environment } from '../../environments/environment';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TasksService {

  constructor(private http: HttpClient) {}

  fetchTasks() {
    return this.http.get<ApiTaskData[]>(environment.apiUrl + '/tasks').pipe(
      map(response => {
        return response.map(taskData => {
          return new Task(
            taskData._id,
            taskData.user,
            taskData.title,
            taskData.status,
          );
        });
      })
    )
  }

  addTask(taskData: TaskData) {
    const body = {
      title: taskData.title,
      user: taskData.user || null
    }

    return this.http.post(environment.apiUrl + '/tasks', body);
  }

  changeTask(id: string, change: {user?: string, status?: string}) {
    return this.http.put<Task>(environment.apiUrl + '/tasks/' + id, change);
  }

  removeTask(id: string) {
    return this.http.delete(environment.apiUrl + `/tasks/${id}`);
  }
}
